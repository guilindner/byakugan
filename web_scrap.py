#!/usr/bin/python

# -*- coding: utf-8 -*-

import time
import os
from selenium import webdriver
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from dotenv import load_dotenv
from sqlalchemy.orm import sessionmaker
import sqlalchemy
from models import Warzone

def login(user,password):
   
    url = 'https://www.ninjamanager.com/account/login'
    browser.get(url)
    time.sleep(1)
    user_form = browser.find_element_by_id('input-login')
    user_form.send_keys(user)
    password_form = browser.find_element_by_id('input-password')
    password_form.send_keys(password)
    password_form.submit()
    time.sleep(1)
    url = 'https://www.ninjamanager.com/'
    browser.get(url)


def inspect():
    
    bars = browser.find_elements_by_class_name('c-bar__text')
    cur_arena = bars[0].text.split("\n/")[0]
    max_arena = bars[0].text.split("\n/")[1]
    cur_world = bars[1].text.split("\n/")[0]
    max_world = bars[1].text.split("\n/")[1]
    resources = browser.find_elements_by_class_name('header-team__resource')
    gold = resources[0].text 
    print(O,"Arena:",P,cur_arena,"/",max_arena,
          O,"   World",P,cur_world,"/",max_world,
          O,"   Gold:",P,gold,W)


zones = {0: "forest-of-death",
         1: "demon-desert",
         2: "land-of-iron",
         3: "coast-of-lightning",
         4: "great-war-plains",
         5: "god-tree"}

def warzone_scan():
    time.sleep(1)
    sep = ' '
    for zone in zones:
        time.sleep(2)
        print(f"Scanning warzone {zones[zone]}")
        url = f'https://www.ninjamanager.com/clans/warzones/{zones[zone]}'
        main_page = browser.get(url)
        try:
            all_bosses = browser.find_elements_by_class_name('p-warzones-grid') #list of enemies names
        except:
            continue
        for boss in all_bosses:
            grid_id = boss.get_attribute('data-gridid')
            if boss.text == 'DEFEATED':
                defeated = 1
                url_img = boss.find_element_by_tag_name("img").get_attribute("src")
                name = url_img.split('/')[-1][:-4].split("-")
                name = sep.join(name).title()
                db_session.query(Warzone).filter(Warzone.grid == grid_id).filter(Warzone.zone == zone).update({Warzone.defeated: 1, Warzone.name: name, Warzone.image: url})
                db_session.commit()

        alive = browser.find_elements_by_class_name('p-warzones-grid__health-fill')
        print(f'Total of {len(alive)} bosses are alive')
        time.sleep(1)
        for boss in range(len(alive)):
            alive = browser.find_elements_by_class_name('p-warzones-grid__health-fill')
            try:
                grid_id = alive[boss].find_element_by_xpath('../../..').get_attribute('data-gridid')
            except:
                continue
            health = alive[boss].get_attribute('style').split()[-1][:-2]
            url_img = alive[boss].find_element_by_xpath('../..').find_element_by_tag_name("img").get_attribute("src")
            name = url_img.split('/')[-1][:-4].split("-")
            name = sep.join(name).title()
            try:
                alive[boss].click()
            except:
                print('exception here, cant click')
                continue
            time.sleep(1)
            button1 = browser.find_elements_by_class_name('c-mission-box__cost')[0]
            button1.click() # click the mission button and enter the boss
            time.sleep(1)
            try:
                items = browser.find_element_by_class_name('p-warzones-rewards').find_elements_by_class_name('c-item__name')
            except:
                continue
            items_name = []
            for item in items:
                items_name.append(item.text)
            items_str = ', '.join(items_name)
            veins = browser.find_element_by_class_name('p-warzones__squares').find_elements_by_class_name('c-item__pic')
            veins_name = []
            for vein in veins:
                url_vein = vein.find_element_by_tag_name("img").get_attribute("src")
                name_vein = url_vein.split('/')[-1][:-4].split("-")
                name_vein = sep.join(name_vein).title()
                veins_name.append(name_vein)
            veins_str = ', '.join(veins_name)
            infos = browser.find_elements_by_class_name('p-warzones-sidebar__info')
            for info in infos:
                if 'Approximately' in info.text:
                    condition = info.text.partition(' ')[2]
                elif 'Battle ends' in info.text:
                    duration = info.text
                elif 'Low Health' in info.text:
                    condition = 'Low Health!'
            temp_boss = db_session.query(Warzone).filter(Warzone.grid == grid_id).filter(Warzone.zone == zone).first()
            if temp_boss.name != name:
                print('Grid ID updated with new boss')
                temp_boss.reported = 0
            print(f'{temp_boss.name}:  {veins_str}')
            temp_boss.health_percentage = health
            temp_boss.name = name
            temp_boss.image = url_img
            temp_boss.defeated = 0
            temp_boss.rewards = items_str
            temp_boss.veins = veins_str
            temp_boss.condition = condition
            temp_boss.duration = duration
            #db_session.query(Warzone).filter(Warzone.grid == grid_id).filter(Warzone.zone == zone).update({Warzone.health_percentage: health, Warzone.name: name, Warzone.image: url_img, Warzone.defeated: 0, Warzone.rewards: items_str, Warzone.veins: veins_str, Warzone.condition: condition, Warzone.duration: duration})
            sub_page = browser.get(url)
            db_session.commit()


if __name__ == '__main__':


    load_dotenv()
    user = os.getenv("NM_USER")
    password = os.getenv("NM_PASS")  # getpass.getpass()
    
    dbuser = os.getenv("DB_USER")
    dbpass = os.getenv("DB_PASS")
    database = os.getenv("DB_NAME")
    dbport = os.getenv("DB_PORT")
    engine = sqlalchemy.create_engine(f"postgres://{dbuser}:{dbpass}@localhost:{dbport}/{database}", echo=False)

    Session = sessionmaker(bind=engine)
    db_session = Session()

    tmin = 1200 # minimum time for the loop, in seconds
    tmax = 1320 # maximum time for the loop, in seconds
        
    opts = Options()
    opts.headless = True
    browser = Chrome(options=opts)
    print(f"Login with {user}")
    login(user,password)
    time.sleep(2)
    while True:
        warzone_scan()
        time.sleep(60)

    #url = f'https://www.ninjamanager.com/clans/warzones/land-of-iron'
    #main_page = browser.get(url)
    #alive = browser.find_elements_by_class_name('p-warzones-grid__health-fill')
