Byakugan
========

Byakugan is a Discord Bot that scans the NinjaManager game.

Current Features
----------------
- Scans for any change in the warzone map
- Report specific warzone returning all bosses that are alive

Initial Setup
-------------

- Update and install required packages (Ubuntu):
```
apt update
apt install postgresql python3-pip python3-psycopg2
```
- Install google-chrome
- Download chromiumdriver and move it to the PATH folder
- Clone repository:
```
git clone https://gitlab.com/guilindner/byakugan.git
cd byakugan
pip install -r requirements.txt --user
```
- Initiate database
```
sudo -u postgres createdb ninjamanager
sudo -u postgres psql
\password postgres
exit
```

- Copy dotenv file to .env and add the required credentials
- Populate initial database:
```
python3 models.py
```

Now using tmux, leave both apps running:
```
tmux
python3 web_scrap.py
python3 byakugan.py
```

For long running term
- cp both nm_byakugan and nm_webscrap to /lib/systemd/system/
```
systemctl enable nm_byakugan
systemctl enable nm_webscrap
```


Maintenance
-----------

Backup database
```
sudo -u postgres pg_dump -F t ninjamanager > backup_2022_04_03.tar
```

Create a local testing database using docker
```
sudo docker run --name some-postgres -e POSTGRES_PASSWORD=test -p 5432:5432 -d postgres
psql -h localhost -U postgres
create database ninjamanager
```

Restore backup
```
pg_restore -U postgres -h localhost -d ninjamanager backup_2022_04_03.tar
```
