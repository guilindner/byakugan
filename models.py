import os
from dotenv import load_dotenv
from sqlalchemy import (
    Boolean,
    Column,
    Integer,
    BigInteger,
    String,
    Float,
    ForeignKey,
)
# from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import sqlalchemy

Base = declarative_base()

# Warzone detection
class Warzone(Base):
    __tablename__ = "warzone"
    id = Column(Integer, primary_key=True)
    grid = Column(Integer)
    zone = Column(Integer)
    name = Column(String)
    defeated = Column(Boolean)
    health_percentage = Column(Integer)
    image = Column(String)
    reported = Column(Boolean)
    rewards = Column(String)
    veins = Column(String)
    condition = Column(String)
    duration = Column(String)

# Queue system
class Player(Base):
    __tablename__ = "player"
    id = Column(Integer, primary_key=True)
    discord_id = Column(BigInteger)
    display_name = Column(String)


class Material(Base):
    __tablename__ = "material"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    url = Column(String)


class Queue(Base):
    __tablename__ = "queue"
    id = Column(Integer, primary_key=True)
    player_id = Column(BigInteger)
    material_id = Column(Integer, ForeignKey("material.id"))
    clan = Column(String)


if __name__ == "__main__":
    load_dotenv()
    print("starting")
    dbuser = os.getenv("DB_USER")
    dbpass = os.getenv("DB_PASS")
    database = os.getenv("DB_NAME")
    dbport = os.getenv("DB_PORT")
    engine = sqlalchemy.create_engine(f"postgres://{dbuser}:{dbpass}@localhost:{dbport}/{database}", echo=True)

    Session = sessionmaker(bind=engine)
    session = Session()
    Base.metadata.create_all(engine)

    # Populate materials
    #with open('materials.txt', 'r') as mat_file:
    #    for mat in mat_file:
    #        name = mat.rstrip()
    #        material = Material(name=name)
    #        session.add(material)
    #session.commit()

    # Populate empty warzones
    grid_size = 450
    for j in range(6):
        for i in range(grid_size):
            test = Warzone(grid=i+j*grid_size, zone=j, reported=0)
            session.add(test)
            print(i, j)

    session.commit()

