import random
import time
import re
import os
from collections import Counter
import sqlalchemy
import difflib
from models import Warzone, Material, Queue, Player
import shlex
from discord.ext import commands, tasks
from discord import Embed, Color
import discord
from sqlalchemy.orm import sessionmaker
from discord.utils import get

from dotenv import load_dotenv

load_dotenv()

bot = commands.Bot(command_prefix="!")

#engine = sqlalchemy.create_engine(f"postgres://byakugansql:ninjamanager", echo=True)
dbuser = os.getenv("DB_USER")
dbpass = os.getenv("DB_PASS")
database = os.getenv("DB_NAME")
dbport = os.getenv("DB_PORT")
engine = sqlalchemy.create_engine(f"postgres://{dbuser}:{dbpass}@localhost:{dbport}/{database}", echo=False)


Session = sessionmaker(bind=engine)

db_session = Session()


def report_new(boss):
    print(boss.name)
    if boss.zone == 0:
        colorzone = discord.Color.green()
    elif boss.zone == 1:
        colorzone = discord.Color.blue()
    elif boss.zone == 2:
        colorzone = discord.Color.red()
    elif boss.zone == 3:
        colorzone = discord.Color.purple()
    elif boss.zone == 4:
        colorzone = discord.Color.gold()
    else:
        colorzone = discord.Color.teal()
    title_boss = f"{boss.name} - WZ{boss.zone+1}"
    embed = discord.Embed(
        title=title_boss,
        #description=f"{role_id.mention}",
        color=colorzone,
    )
    health = boss.condition.split(' ')[2]
    #embed.set_author(name="boss")
    embed.set_thumbnail(url=boss.image)
    embed.add_field(
        name="*Total Health*",
        value=f"{health}",
        inline=True,
    )
    embed.add_field(
        name="*Duration*",
        value=f"{boss.duration}",
        inline=True,
    )
    embed.add_field(
        name="*Rewards*",
        value=f"-{boss.rewards}"
    )
    return embed

def report_boss(boss):
    print(boss.name)
    if boss.zone == 0:
        colorzone = discord.Color.green()
    elif boss.zone == 1:
        colorzone = discord.Color.blue()
    elif boss.zone == 2:
        colorzone = discord.Color.red()
    elif boss.zone == 3:
        colorzone = discord.Color.purple()
    elif boss.zone == 4:
        colorzone = discord.Color.gold()
    else:
        colorzone = discord.Color.teal()
    title_boss = f"{boss.name} - WZ{boss.zone+1}"
    embed = discord.Embed(
        title=title_boss,
        #description=f"{role_id.mention}",
        color=colorzone,
    )
    #embed.set_author(name="boss")
    embed.set_thumbnail(url=boss.image)
    embed.add_field(
        name="*Condition*",
        value=f"{boss.condition}",
        inline=True,
    )
    embed.add_field(
        name="*Duration*",
        value=f"{boss.duration}",
        inline=True,
    )
    embed.add_field(
        name="*Rewards*",
        value=f"-{boss.rewards}"
    )
    embed.add_field(
        name="*Veins*",
        value=f"-{boss.veins}"
    )
    return embed

def print_dict(dct):
    printed = []
    #for item, amount in dct.items():  # dct.iteritems() in Python 2
    for i in dct.most_common():
        printed.append("{} ({})\n".format(i[0], i[1]))
    return printed

@bot.command(help="Report boss status", brief="Show all alive bosses from a specific zone")
async def status(ctx, *, arg="0"):
    await ctx.channel.send("The current enemies on the battlefield are:")
    if arg == "0":
        for zone in range(6):
            await ctx.channel.send(f"--- *Warzone {zone+1}* ---")
            query_boss = db_session.query(Warzone).filter(Warzone.name != '', Warzone.defeated == False, Warzone.zone == zone)
            for boss in query_boss:
                result = report_boss(boss)
                await ctx.channel.send(embed=result)
            time.sleep(2)

    else:
        await ctx.channel.send(f"--- *Warzone {arg}* ---")
        query_boss = db_session.query(Warzone).filter(Warzone.name != '', Warzone.defeated == False, Warzone.zone == int(arg)-1)
        for boss in query_boss:
            result = report_boss(boss)
            await ctx.channel.send(embed=result)

@bot.command()
async def veins(ctx, *, arg="0"):
    await ctx.channel.send("The current veins on the battlefield are:")
    if arg == "0":
        for zone in range(6):
            await ctx.channel.send(f"--- *Warzone {zone+1}* ---")
            query_boss = db_session.query(Warzone).filter(Warzone.name != '', Warzone.defeated == False, Warzone.zone == zone, Warzone.veins != '')
            for boss in query_boss:
                result = report_boss(boss)
                await ctx.channel.send(embed=result)
            time.sleep(2)
    else:
        await ctx.channel.send(f"--- *Warzone {arg}* ---")
        query_boss = db_session.query(Warzone).filter(Warzone.name != '', Warzone.defeated == False, Warzone.zone == int(arg)-1, Warzone.veins != '')
        for boss in query_boss:
            result = report_boss(boss)
            await ctx.channel.send(embed=result)



@tasks.loop(seconds=14400)
async def periodic_report():
    channel = bot.get_channel(837873317012045824)
    await channel.send("Periodic report")
    query_boss = db_session.query(Warzone).filter(Warzone.name != '', Warzone.defeated == 0)
    await channel.send("The current enemies on the battlefield are:")
    for boss in query_boss:
        result = report_new(boss)
        await channel.send(embed=result)

@tasks.loop(seconds=45)
async def mytask():
    print("searching for new bosses...")
    await bot.wait_until_ready()
    channel = bot.get_channel(837873317012045824)
    role_id = get(channel.guild.roles, name='SAIZENSEN NO SENSHI')
    db_session = Session()
    query_new = db_session.query(Warzone).filter(Warzone.reported == False, Warzone.name != '', Warzone.defeated == False)
    for boss in query_new:
        print("New boss found!")
        await channel.send(f"A new danger has appeared on the battlefield! {role_id.mention}")
        result = report_new(boss)
        await channel.send(embed=result)
        boss.reported = 1
    db_session.commit()

# Queue system

def get_url(material):
    url = "https://www.ninjamanager.com/img/material/large/"
    url += material.name.replace(' ','-').lower()
    url += ".png"
    return url

def show_queue(material, clan, players):
    image_url = get_url(material)
    if clan == 'GG':
        colorzone = discord.Color.gold()
    else:
        colorzone = discord.Color.red()
    embed = discord.Embed(
        title=f"{material.name} ({clan})",
        #description=f"GG",
        color=colorzone,
    )
    #embed.set_author(name="boss")
    embed.set_thumbnail(url=image_url)
    for i,entry in enumerate(players):
        embed.add_field(
            name=f"*{i+1}*",
            value=f"{entry}",
            inline=True,
        )
    return embed

def fix_inputs(arg):
    clan = arg.split()[0].upper()
    material = arg.split()[1:]
    material = ' '.join(material)
    if clan not in clans:
        #await ctx.channel.send(f"The first argument must be GG or HG.")
        return
    material_name = difflib.get_close_matches(material, material_list)
    if not material_name:
        #await ctx.channel.send(f"Material **{material}** not found.")
        return
    material_name = material_name[0]
    return clan, material


@bot.command()
async def join(ctx, *, arg=''):
    """ !join celestial_dye"""
    query_player = db_session.query(Player).filter(Player.discord_id == ctx.author.id).first()
    if not query_player:  # Add player to database with display_name if it's the first time
        db_session.add(Player(discord_id = ctx.author.id, display_name = ctx.author.display_name))
        db_session.commit()
    #clan, material_name = fix_inputs(arg)
    #print(clan, material_name)
    clan = arg.split()[0].upper()
    material = arg.split()[1:]
    material = ' '.join(material)
    if clan not in clans:
        await ctx.channel.send(f"The first argument must be GG or HG.")
        return
    material_name = difflib.get_close_matches(material, material_list)
    if not material_name:
        await ctx.channel.send(f"Material **{material}** not found.")
        return
    material_name = material_name[0]
    query_material = db_session.query(Material).filter(Material.name == material_name).all()[0]
    query_queue = db_session.query(Queue).filter(Queue.player_id == ctx.author.id).filter(Queue.clan == clan).filter(Queue.material_id == query_material.id).first()
    if query_queue:
        await ctx.channel.send(f"You are already in that list.")
        return
    if not query_material:
        await ctx.channel.send(f"Material **{material}** not found.")
    else:
        queue = Queue()
        queue.material_id = query_material.id
        queue.player_id = ctx.author.id
        queue.clan = clan
        db_session.add(queue)
        db_session.flush()
        db_session.commit()
        await ctx.channel.send(f"**{ctx.author.display_name}** has joined the **{clan}** queue for **{material_name}**")
        await ctx.invoke(bot.get_command('show'), arg=arg)


@bot.command()
async def show(ctx, *, arg=''):
    if arg == '':
        await ctx.channel.send(f"The first argument must be GG or HG.")
        return
    clan = arg.split()[0].upper()
    material = arg.split()[1:]
    material = ' '.join(material)
    if clan not in clans:
        await ctx.channel.send(f"The first argument must be GG or HG.")
        return
    material_name = difflib.get_close_matches(material, material_list)
    if not material_name:
        await ctx.channel.send(f"Material **{material}** not found.")
        return
    material_name = material_name[0]
    query_material = db_session.query(Material).filter(Material.name == material_name)
    material_selected = query_material.first()
    if material_selected:
        query_queue = db_session.query(Queue).filter(Queue.material_id == material_selected.id).order_by(Queue.id)
        players = []
        for entry in query_queue:
            if entry.clan == clan:
                query_player = db_session.query(Player).filter(Player.discord_id == entry.player_id).first()
                players.append(query_player.display_name)
        result = show_queue(material_selected, clan, players)
        await ctx.channel.send(embed=result)


@bot.command()
async def leave(ctx, *, arg=''):
    """ !leave GG celestial dye"""
    if arg == '':
        await ctx.channel.send(f"The first argument must be GG or HG.")
        return
    clan = arg.split()[0].upper()
    material = arg.split()[1:]
    material = ' '.join(material)
    if clan not in clans:
        await ctx.channel.send(f"The first argument must be GG or HG.")
        return
    material_name = difflib.get_close_matches(material, material_list)
    if not material_name:
        await ctx.channel.send(f"Material **{material}** not found.")
    material_name = material_name[0]
    query_material = db_session.query(Material).filter(Material.name == material_name)
    query_queue = db_session.query(Queue).filter(Queue.player_id == ctx.author.id).filter(Queue.material_id == query_material[0].id).filter(Queue.clan == clan).delete()
    await ctx.channel.send(f"**{ctx.author.display_name}** has left the **{clan}** queue for **{material_name}**")
    db_session.commit()



@bot.command()
async def claim(ctx, *, arg=''):
    """ !claim GG celestial_dye"""
    if arg == '':
        await ctx.channel.send(f"The first argument must be GG or HG.")
        return
    player_name = ctx.author.display_name
    material_name = difflib.get_close_matches(arg, material_list)[0]

    clan = arg.split()[0].upper()
    material = arg.split()[1:]
    material = ' '.join(material)

    await ctx.channel.send(f"**{player_name}** has claimed **{material}** ({clan}).")
    await ctx.invoke(bot.get_command('leave'), arg=arg)
    await ctx.invoke(bot.get_command('join'), arg=arg)

@bot.command()
async def listall(ctx):
    content = (', '.join(material_list))
    divider = int(len(material_list)/2)

    await ctx.channel.send(', '.join(material_list[:divider]))
    await ctx.channel.send(', '.join(material_list[divider:]))

@bot.command()
async def listhot(ctx, *, arg=''):
    if arg == '':
        await ctx.channel.send(f"The first argument must be GG or HG.")
        return
    clan = arg.split()[0].upper()
    if clan not in clans:
        await ctx.channel.send(f"The first argument must be GG or HG.")
        return
    query_queue = db_session.query(Queue).filter(Queue.clan == clan).all()
    hotlist = []
    for entry in query_queue:
        query_material = db_session.query(Material).filter(Material.id == entry.material_id).first()
        hotlist.append(query_material.name)
    counted = Counter(hotlist)
    nice = print_dict(counted)
    await ctx.channel.send(f'Most queued items for {clan}:')
    await ctx.channel.send(''.join(nice))

@bot.command()
async def listme(ctx, *, arg=''):
    for clan in clans:
        query_queue = db_session.query(Queue).filter(Queue.clan == clan).all()
        my_queues = []
        for entry in query_queue:
            if entry.player_id == ctx.author.id:
                query_material = db_session.query(Material).filter(Material.id == entry.material_id).first()
                my_queues.append(query_material.name)
        counted = Counter(my_queues)
        #nice = print_dict(counted)
        final = list(counted)
        await ctx.channel.send(f'{clan} list:')
        await ctx.channel.send(', '.join(final))

@bot.command()
async def rename(ctx, *, arg=''):
    player = db_session.query(Player).filter(Player.discord_id == ctx.author.id).first()
    old_name = player.display_name
    if arg == '':
        new_name = ctx.author.display_name
    else:
        new_name = arg
    player.display_name = new_name
    db_session.commit()
    await ctx.channel.send(f'{old_name} is now {new_name}')

mytask.start()

clans = ['GG', 'HG']
material_list = []
with open('/home/guinux/byakugan/materials.txt', 'r') as f:
    for row in f:
        material_list.append(row.rstrip())
material_list.sort()

Session = sessionmaker(bind=engine)
db_session = Session()

bot.run(os.getenv("DISCORD_TOKEN"))

